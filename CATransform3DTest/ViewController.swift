//
//  ViewController.swift
//  CATransform3DTest
//
//  Created by Kelly Chu on 5/15/19.
//  Copyright © 2019 ISBX. All rights reserved.
//

import UIKit

extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}

class ViewController: UIViewController, UITextFieldDelegate {
    let AXIS_LENGTH = 100;
    var xRot: CGFloat = 0
    var yRot: CGFloat = 0
    var zRot: CGFloat = 0

    let cube = CATransformLayer()
    let vectorLine = CAShapeLayer()
    
    var axisX = CAShapeLayer()
    var axisY = CAShapeLayer()
    var axisZ = CAShapeLayer()
    
    @IBOutlet weak var xSlider: UISlider!
    @IBOutlet weak var ySlider: UISlider!
    @IBOutlet weak var zSlider: UISlider!
    @IBOutlet weak var xRotation: UITextField!
    @IBOutlet weak var yRotation: UITextField!
    @IBOutlet weak var zRotation: UITextField!
    @IBOutlet weak var rMagnitude: UITextField!
    @IBOutlet weak var theta: UITextField!
    @IBOutlet weak var phi: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.xRotation.delegate = self
        self.yRotation.delegate = self
        self.zRotation.delegate = self
        self.initializeAxes()
        self.initAxisPlanes()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
//        self.drawVector()
//        self.xRot = CGFloat(30)
//        self.yRot = CGFloat(-317)
        self.initializeControls()
        self.rotate()
        
//        self.drawVector()
//        rotate()
    }
    
    func initializeControls() {
        self.xSlider.value = Float(self.xRot)
        self.ySlider.value = Float(self.yRot)
        self.zSlider.value = Float(self.zRot)
        self.xRotation.text = self.xRot.description
        self.yRotation.text = self.yRot.description
        self.zRotation.text = self.zRot.description
    }
    
    func initializeAxes() {
        // horizontal
        self.xSlider.tintColor = UIColor.red
        self.axisX = self.createAxis(color: self.xSlider.tintColor.cgColor, transform: CATransform3DIdentity)
        cube.addSublayer(self.axisX)
        // rotate 90deg on z axis, vertical
        self.ySlider.tintColor = UIColor.green
        self.axisY = self.createAxis(color: self.ySlider.tintColor.cgColor, transform: CATransform3DRotate(CATransform3DIdentity, CGFloat(90).degreesToRadians, 0, 0, 1))
        cube.addSublayer(self.axisY)
        // rotate 90deg on y axis, orthogonal to xy plane
        self.zSlider.tintColor = UIColor.blue
        self.axisZ = self.createAxis(color: self.zSlider.tintColor.cgColor, transform: CATransform3DRotate(CATransform3DIdentity, CGFloat(90).degreesToRadians, 0, 1, 0))
        cube.addSublayer(self.axisZ)
        // now position the transform layer in the center
        cube.position = CGPoint(x: view.bounds.midX, y: view.bounds.midY)
        // and add the cube to our main view's layer
        view.layer.addSublayer(cube)
    }
    
    func initAxisPlanes() {
        let xyPlane = self.createAxisPlane(rect: CGRect(x: -AXIS_LENGTH, y: -AXIS_LENGTH, width: 2*AXIS_LENGTH, height: 2*AXIS_LENGTH), color: UIColor.red)
        let xLabel = self.createAxisLabel(string: "X", frame: CGRect(x: AXIS_LENGTH-20, y: 0, width: 20, height: 20), color: UIColor.black)
        xyPlane.addSublayer(xLabel)
        let yLabel = self.createAxisLabel(string: "Y", frame: CGRect(x: 0, y: -AXIS_LENGTH, width: 20, height: 20), color: UIColor.black)
        xyPlane.addSublayer(xLabel)
        xyPlane.addSublayer(yLabel)
        cube.addSublayer(xyPlane)
        
        let yzPlane = self.createAxisPlane(rect: CGRect(x: -AXIS_LENGTH, y: -AXIS_LENGTH, width: 2*AXIS_LENGTH, height: 2*AXIS_LENGTH), color: UIColor.green)
        yzPlane.transform = CATransform3DRotate(CATransform3DIdentity, CGFloat(90).degreesToRadians, 0, 1, 0)
        let zLabel = self.createAxisLabel(string: "Z", frame: CGRect(x: -AXIS_LENGTH, y: 0, width: 20, height: 20), color: UIColor.black)
        yzPlane.addSublayer(zLabel)
        cube.addSublayer(yzPlane)
        
        let zxPlane = self.createAxisPlane(rect: CGRect(x: -AXIS_LENGTH, y: -AXIS_LENGTH, width: 2*AXIS_LENGTH, height: 2*AXIS_LENGTH), color: UIColor.blue)
        zxPlane.transform = CATransform3DRotate(CATransform3DIdentity, CGFloat(90).degreesToRadians, 1, 0, 0)
        cube.addSublayer(zxPlane)
    }
    
    func createAxisPlane(rect: CGRect, color: UIColor) -> CAShapeLayer {
        let plane = CAShapeLayer()
        plane.path = UIBezierPath(rect: rect).cgPath
        plane.fillColor = color.cgColor
        plane.opacity = 0.3
        return plane
    }
    
    func createAxisLabel(string: String, frame: CGRect, color: UIColor) -> CATextLayer {
        let label = CATextLayer()
        label.frame = frame
        label.string = string
        label.font = CFBridgingRetain(UIFont.boldSystemFont(ofSize: 18).fontName)
        label.fontSize = 18
        label.foregroundColor = color.cgColor
        label.alignmentMode = CATextLayerAlignmentMode.center
        return label
    }
    
    func createAxis(color: CGColor, transform: CATransform3D) -> CAShapeLayer {
        let axis = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: CGPoint(x:-AXIS_LENGTH, y: 0))
        linePath.addLine(to: CGPoint(x: AXIS_LENGTH, y: 0))

        axis.path = linePath.cgPath
        axis.fillColor = color
        axis.opacity = 1.0
        axis.strokeColor = color
        axis.transform = transform
        return axis;
    }
    
    @IBAction func plotButtonDidPress(_ sender: Any) {
        self.drawVector()
    }
    
    func drawVector() {
        let r = self.stringToFloat(val: self.rMagnitude.text!)
        // negate since iOS rotation is reversed (clockwise)
        // theta = directed angle between positive x-axis and the vector ray (rotation about the z-axis
        let theta = self.stringToFloat(val: self.theta.text!)
        // phi = nondirected angle between the positive z-axis and the vector ray
        let phi = self.stringToFloat(val: self.phi.text!) // phi =
        
        
        vectorLine.position = CGPoint(x: cube.bounds.midX, y: cube.bounds.midY)
        let linePath = UIBezierPath()
        linePath.move(to: CGPoint(x: 0, y: 0))
        linePath.addLine(to: CGPoint(x: r, y: 0))
        vectorLine.path = linePath.cgPath
        vectorLine.fillColor = nil
        vectorLine.opacity = 1.0
        vectorLine.strokeColor = UIColor.black.cgColor
        // vector gets initialized at (r, theta = 0, phi = 90 or pi/2) parallel or coincidental to x-axis
        vectorLine.transform = CATransform3DIdentity
        // rotate about y-axis by 90 to correct initial orientation
        vectorLine.transform = CATransform3DRotate(vectorLine.transform, CGFloat(-90).degreesToRadians, 0, r, 0)
        
        vectorLine.transform = CATransform3DRotate(vectorLine.transform, phi.degreesToRadians, 0, r, 0)
        // calculate the new axis of rotation after rotating about y
        let y1 = CGFloat(0) // y1 is zero since vector is on xz plane
        let x1 = r*cos(phi.degreesToRadians)
        let z1 = r*sin(phi.degreesToRadians)
        vectorLine.transform = CATransform3DRotate(vectorLine.transform, -theta.degreesToRadians, x1, y1, z1)
        cube.addSublayer(vectorLine)
    }
    
    func rotate() {
        cube.transform = CATransform3DIdentity // reset before applying new rotation
        // negate angle to rotate counter-clockwise on x-axis
        cube.transform = CATransform3DRotate(cube.transform, -self.xRot.degreesToRadians, 1, 0, 0)
        // since y axis on iOS is inversed, no need to negate
        cube.transform = CATransform3DRotate(cube.transform, self.yRot.degreesToRadians, 0, 1, 0)
        // negate angle to rotate counter-clockwise about z-axis
        cube.transform = CATransform3DRotate(cube.transform, -self.zRot.degreesToRadians, 0, 0, 1)
    }
    
    func floatToString(val: CGFloat) -> String {
        #if arch(x86_64) || arch(arm64)
        return ceil(val).description
        #else
        return ceilf(val).description
        #endif
    }

    @IBAction func xSliderChanged(_ sender: UISlider) {
        self.xRot = CGFloat(sender.value)
        self.xRotation.text = self.floatToString(val: self.xRot)
        print("self.x \(self.xRot)")
        self.rotate()
    }
    @IBAction func ySliderChanged(_ sender: UISlider) {
        self.yRot = CGFloat(sender.value)
        self.yRotation.text = self.floatToString(val: self.yRot)
        print("self.y \(self.yRot)")
        self.rotate()
    }
    @IBAction func zSliderChanged(_ sender: UISlider) {
        self.zRot = CGFloat(sender.value)
        self.zRotation.text = self.floatToString(val: self.zRot)
        print("self.z \(self.zRot)")
        self.rotate()
    }
    @IBAction func xRotationChanged(_ sender: UITextField) {
        self.xRot = self.stringToFloat(val: sender.text!)
        self.xSlider.value = Float(self.xRot)
        print("self.x \(self.xRot)")
        self.rotate()
    }
    @IBAction func yRotationChanged(_ sender: UITextField) {
        self.yRot = self.stringToFloat(val: sender.text!)
        self.ySlider.value = Float(self.yRot)
        print("self.y \(self.yRot)")
        self.rotate()
    }
    @IBAction func zRotationChanged(_ sender: UITextField) {
        self.zRot = self.stringToFloat(val: sender.text!)
        self.zSlider.value = Float(self.zRot)
        print("self.z \(self.zRot)")
        self.rotate()
    }
    
    func stringToFloat(val: String) -> CGFloat {
        guard let n = NumberFormatter().number(from: val) else { return 0 }
        return CGFloat(truncating: n)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet(charactersIn:"+-.0123456789")
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        let stringVal = textField.text!
        let val = (Float(stringVal) != nil) ? Float(stringVal) : Float(0)
        if val!.isLess(than: self.xSlider.minimumValue) {
            textField.text = self.floatToString(val: CGFloat(self.xSlider?.minimumValue ?? -360))
        } else if self.xSlider.maximumValue.isLess(than: val!) {
            textField.text = self.floatToString(val: CGFloat(self.xSlider?.maximumValue ?? 360))
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        let val = Float(textField.text!)!
        return val.isLessThanOrEqualTo(self.xSlider.maximumValue) && self.xSlider.minimumValue.isLessThanOrEqualTo(val)
    }

}

